data "aws_vpc" "default" {
  default = true
}

output "defaultvpcid" {
  value = data.aws_vpc.default.id
}

data "aws_security_group" "default" {
  vpc_id = data.aws_vpc.default.id
}

output "aws_security_group_id" {
  value = data.aws_security_group.default.id
}
